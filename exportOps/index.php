<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<?php

?>

<script type="text/javascript" language="javascript" src="../datatable/media/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../datatable/media/js/jquery.dataTables.js"></script>

<table cellpadding="0" cellspacing="0" border="0" class="display" id="operaciones">
  <thead>
    <tr>
      <th width="5.6%"></th><th width="11.8%">Programa</th><th width="11.8%">Estado</th><th width="11.8%">Municipio</th><th width="11.8%">Tipo de beneficiario</th><th width="11.8%">Concepto 1</th><th width="11.8%">Concepto 2</th><th width="11.8%">Monto</th><th width="11.8%">Año</th></tr>
  </thead>
  <tbody><tr class="odd"><td class=" sorting_1">19937</td><td class="">Desarrollo Forestal</td><td class="">PUEBLA</td><td class="">Chiautla</td><td class="">Propiedad Privada</td><td class="">A2.2 Prácticas de Manejo para Aprovechamientos No Maderables  y de Vida Silvestre</td><td class="">Prácticas de manejo para aprovechamientos no maderables  </td><td class="">9000</td><td class="">2010</td></tr><tr class="even"><td class=" sorting_1">19936</td><td class="">Desarrollo Forestal</td><td class="">MICHOACÁN</td><td class="">Hidalgo</td><td class="">Propiedad Privada</td><td class="">A1.2 Programa de Manejo Forestal Maderable</td><td class="">Programa De Manejo Forestal Maderable</td><td class="">9017.25</td><td class="">2010</td></tr><tr class="odd"><td class=" sorting_1">19935</td><td class="">Desarrollo Forestal</td><td class="">DURANGO</td><td class="">Ocampo</td><td class="">Propiedad Privada</td><td class="">A2.2 Prácticas de Manejo para Aprovechamientos No Maderables  y de Vida Silvestre</td><td class="">Prácticas de manejo para aprovechamientos no maderables  </td><td class="">9020</td><td class="">2010</td></tr><tr class="even"><td class=" sorting_1">19934</td><td class="">Desarrollo Forestal</td><td class="">DURANGO</td><td class="">San Bernardo</td><td class="">Propiedad Social</td><td class="">A2.2 Prácticas de Manejo para Aprovechamientos No Maderables  y de Vida Silvestre</td><td class="">Prácticas de manejo para aprovechamientos no maderables  </td><td class="">9065</td><td class="">2010</td></tr><tr class="odd"><td class=" sorting_1">19933</td><td class="">Desarrollo Forestal</td><td class="">SONORA</td><td class="">Yécora</td><td class="">Propiedad Privada</td><td class="">A1.4 Plan de Manejo de Vida Silvestre</td><td class="">Prácticas de manejo para aprovechamientos de la vida silvestre  </td><td class="">9075</td><td class="">2010</td></tr>

    <tr id="suma" class=""><td width="5.6%"></td><td width="11.8%"></td><td width="11.8%"></td><td width="11.8%"></td><td width="11.8%"></td><td width="11.8%"></td><td width="11.8%">Suma:</td><td width="11.8%">45177.25</td><td width="11.8%"></td></tr>
    <tr id="promedio" class=""><td width="5.6%"></td><td width="11.8%"></td><td width="11.8%"></td><td width="11.8%"></td><td width="11.8%"></td><td width="11.8%"></td><td width="11.8%">Promedio:</td><td width="11.8%">9035.45</td><td width="11.8%"></td></tr>
  </tbody>
  <tfoot>
  </tfoot>

</table>

<script type="text/javascript" charset="utf-8">
 jQuery(document).ready(function($){

   var trrs = Array();
   $('#operaciones tr').each(function(){
     var tr = $(this);
     var trr = Array();
     tr.find('td').each(function(){
       var td = $(this);
       trr.push( td.html() );
     });
     trrs.push( trr );     
   });
   console.log( trrs );


   $.ajax({
     type: "POST",
     url: "ajaxhandler.php",
     data: { operaciones: trrs },
     success:function( data ) {
       console.log( " succcess:: " , data );
       $('body').append($('<a>').attr('href',data).html('Descargar'));
     }
   });
 });
 
</script>

<?php

?>