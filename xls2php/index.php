<?php
/*
Cargando un archivo .xls con PHP
*/

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

  /** Include PHPExcel_IOFactory */
  require_once dirname(__FILE__) . '/PHPExcel/Classes/PHPExcel/IOFactory.php';

  $archivo = "Base2010Final.xls";

  if (!file_exists( $archivo )) {
    exit("no existe el archivo." . EOL);
}

$callStartTime = microtime(true);
echo $callStartTime , " cargar: ", $archivo, EOL;

echo EOL;

$objPHPExcel = PHPExcel_IOFactory::load($archivo);

foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
  echo 'Hoja - ' , $worksheet->getTitle() , EOL;

  foreach ($worksheet->getRowIterator() as $row) {
    echo 'Hilera - ' , $row->getRowIndex() , EOL;

    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(false);
    
    foreach ($cellIterator as $cell) {
      if (!is_null($cell)) {
        echo '        Celda - ' , $cell->getCoordinate() , ' - ' , $cell->getCalculatedValue() , EOL;
      }
    }
  }
}

$callEndTime = microtime(true);


echo EOL;
echo $callEndTime , " listo ", EOL;

echo EOL;
echo "duracion: ",(float) ($callEndTime - $callStartTime), EOL;

?>