<?php

function log_error($texto,$echo=true)
{
	$errorFile = getcwd();
	$errorFile = substr($errorFile,0,strpos($errorFile, "ccmss"));
	if(strpos(strtolower(PHP_OS),"win")!==false)
	{
		$errorFile.="ccmss\\logs\\";
	}
	else
	{
		$errorFile.="ccmss/logs/";
	}
	$errorFile.="error.txt";
	//$errorFile = "/var/www/html/ccmss/logs/error.txt"; //Ubicación en foocoop.mx
    //$errorFile = "/opt/lampp/htdocs/ccmss/logs/error.txt";
	//$errorFile = "C:\\xampp\\htdocs\\ccmss\\logs\\error.txt"; //ruta HARDCODEADA pora asuntos de debugging
  	date_default_timezone_set('America/Mexico_City');
  	$date = date('d/m/Y h:i:s a', time());
  	file_put_contents($errorFile, $date." ".$texto."\r\n",FILE_APPEND);
    if($echo)
		echo("Ha ocurrido un error en la página. Por favor póngase en contacto con el administrador.");
  	die();
}

function log_debug($texto)
{
	$errorFile = getcwd();
	$errorFile = substr($errorFile,0,strpos($errorFile, "ccmss"));
	if(strpos(strtolower(PHP_OS),"win")!==false)
	{
		$errorFile.="ccmss\\logs\\";
	}
	else
	{
		$errorFile.="ccmss/logs/";
	}
	$errorFile.="debug.txt";
  	date_default_timezone_set('America/Mexico_City');
  	$date = date('d/m/Y h:i:s a', time());
  	file_put_contents($errorFile, $date." ".$texto."\r\n",FILE_APPEND);
}

?>
