<?php

global $Sql,$sTable,$sIndexColumn;

require_once(getcwd() . "../globales/auth.php");
require_once(__DIR__ . "/../globales/campos.php");

require_once("../scripts/errorLog.php");
/* 
 * Local functions
 */
/*function fatal_error ( $sErrorMessage = '' )
{
  header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
  die( $sErrorMessage );
}*/

/* 
 * MySQL connection
 */
if ( ! $Sql['link'] = mysql_pconnect( $Sql['server'], $Sql['user'], $Sql['password']  ) )
{
  log_error( 'Could not open connection to server',false );
}


mysql_set_charset('utf8');


if ( ! mysql_select_db( $Sql['db'], $Sql['link'] ) )
{
  log_error( 'Could not select database ',false );
}

/* 
 * Paging
 */
$sLimit = "";
$speedLimit = 100; //Límite puesto para acelerar los querys

if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
{
  $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
  intval( $_GET['iDisplayLength'] );
}

//	$sLimit = "LIMIT 1, 10";
/*
 * Ordering
 */

$sOrder = "";
/*
if ( isset( $_GET['iSortCol_0'] ) )
{
$sOrder = "ORDER BY  ";
for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
{
if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
{
$sOrder .= "`".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."` ".
($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
}
}

$sOrder = substr_replace( $sOrder, "", -2 );
if ( $sOrder == "ORDER BY" )
{
$sOrder = "";
}
}
 */	

/* 
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */


function fixAccents($fixStr) {
  $fixStr = str_replace('á','a',$fixStr);
  $fixStr = str_replace('é','e',$fixStr);
  $fixStr = str_replace('í','i',$fixStr);
  $fixStr = str_replace('ó','o',$fixStr);
  $fixStr = str_replace('ú','u',$fixStr);
  $fixStr = str_replace('Á','A',$fixStr);
  $fixStr = str_replace('É','E',$fixStr);
  $fixStr = str_replace('Í','I',$fixStr);
  $fixStr = str_replace('Ó','O',$fixStr);
  $fixStr = str_replace('Ú','U',$fixStr);
  return $fixStr;
}

$sWhere = "";

echo count($aColumns);

if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
{
  $sWhere = "WHERE (";
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
      if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" )
      {
        $search = fixAccents($_GET['sSearch']);

        $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string( $search )."%' OR ";
      }
    }
    $sWhere = substr_replace( $sWhere, "", -3 );
    $sWhere .= ')';
}

/* Individual column filtering */
for ( $i=0 ; $i<count($aColumns) ; $i++ )
{
  // introducir restricción numérica para la columna "monto" (no. 6)
  if( $aColumns[$i] != "monto" ) {
    if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
    {
      // cuando está seteado customsearch

      if(isset($_GET['customSearch_'.$i]) && $_GET['customSearch_'.$i] != "" ){
        $customSearch = $_GET['customSearch_'.$i];
        
        if ( $sWhere == "" )
        {
          $sWhere = "WHERE ";
        }        
        else
        {
          $sWhere .= " AND ";
        }

        $searchArr = explode( ",", $customSearch );

        $sWhere .= "(";

          for($j = 0; $j < count($searchArr); $j++ ) {

            $searchTerm = fixAccents( $searchArr[$j] );
            $searchTerm = str_replace("``","'",$searchArr[$j] );
            if( $j > 0 ) {
              $sWhere .= 'OR ';
            }
            $sWhere .= $aColumns[$i] . "='" . mysql_real_escape_string( $searchTerm ) . "' ";

          }

          $sWhere .= ")";


//$sWhere .= $customSearch;

      } else {
  // cuando está vacío customsearch
        $search = fixAccents($_GET['sSearch_'.$i]); 
        if ( $sWhere == "" )
        {
          $sWhere = "WHERE ";
        }
        else
        {
          $sWhere .= " AND ";
        }
        $sWhere .= "`".$aColumns[$i]."` = '".mysql_real_escape_string($search)."' ";
      }
    }
  } else {
    if( isset($_GET['maxMonto'] ) ) {
      $max = $_GET['maxMonto'];
    } else {
      $max = 10000000000;
    }
    if( isset($_GET['minMonto'] ) ) {
      $min = $_GET['minMonto'];
    } else {
      $min = 0;
    }  
    if ( $sWhere == "" ) {
      $sWhere = "WHERE ";
    } else {
      $sWhere .= " AND ";
    }
    $sWhere .= "monto BETWEEN " .$min." AND ".$max;  
  }
}




//file_put_contents( "../debug.txt", $sWhere . "\n", FILE_APPEND );

 /*
 * SQL queries
 * Get data to display
 */

$iFilteredTotal=0;
$iTotal=0;
$datosQuery="";
$sQuery = "SHOW TABLES from ".$Sql['db'];
$tablas=mysql_query($sQuery);
/*$numTablas=mysql_num_rows($tablas);
$numRegs = $numTablas*$speedLimit;
$indice = str_replace("LIMIT","", $sLimit);
$largo = trim(substr($indice, strpos($indice,",")+1));
$indice = trim(substr($indice, 0,strpos($indice, ",")));
$limite=0;
if(($indice+$largo)>$numRegs)
  $limite=floor(($indice+$largo)/$numRegs);
$limite="limit ".$limite*$numRegs.", ".$numRegs;*/



while($tableRow = mysql_fetch_row($tablas))
{
  $tabla=$tableRow[0];
  /* Total data set length */
  $sQuery = "SELECT COUNT(`".$sIndexColumn."`) FROM   $tabla";

  $rResultTotal = mysql_query( $sQuery, $Sql['link'] ) or log_error( 'Error en el cálculo de resultados fitrados. MySQL Error: ' . mysql_errno(),false );


  $aResultTotal = mysql_fetch_array($rResultTotal);
  var_dump( $aResultTotal );
  $iTotal += $aResultTotal[0];
  $datosQuery .= "SELECT `".str_replace(" , ", " ", implode("`, `", $aColumns));
  $datosQuery.="`
      FROM   $tabla
      $sWhere
      $sOrder";
      //$datosQuery.=$limite;
      //log_debug("exp: ".$datosQuery.$limite);
      //LIMIT 0, 100";
      //$speedLimit;
  $datosQuery.=' UNION ';
}
$datosQuery = str_replace("`monto`", "FORMAT(monto,0)", $datosQuery);
$datosQuery = substr($datosQuery,0,strrpos($datosQuery, 'UNION'));
$datosQuery = "Select SQL_CALC_FOUND_ROWS * from (".$datosQuery;
$datosQuery.=") joined ".$sLimit;
log_debug($datosQuery);

$rResult = mysql_query( $datosQuery, $Sql['link'] ) or log_error( 'Error en la cosulta de todos los registros de todas las tablas. MySQL Error: ' . mysql_errno(),false );
$sQuery = "SELECT FOUND_ROWS()";

$rResultFilterTotal = mysql_query( $sQuery, $Sql['link'] ) or log_error( 'Error en el filtrado de registros. MySQL Error: ' . mysql_errno(),false );
$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
log_debug("filtrados: ".$aResultFilterTotal[0]);
$iFilteredTotal += $aResultFilterTotal[0];

/*
 * Output
 */
$output = array(
  "sEcho" => intval($_GET['sEcho']),
  "iTotalRecords" => $iTotal,
  "iTotalDisplayRecords" => $iFilteredTotal,
  "aaData" => array()
);


while ( $aRow = mysql_fetch_array( $rResult ) )
{
  $row = array();
  for ( $i=0 ; $i<count($aColumns) ; $i++ )
  {
    if($aColumns[$i]!="monto")
    {
      $row[] = str_replace( "?", " ", $aRow[ $aColumns[$i] ] );
    }
    else
    {
      $row[] = str_replace( "?", " ", $aRow["FORMAT(monto,0)"] );
    }
  }
  $output['aaData'][] = $row;
}

$output = 123;
echo json_encode( $output );

?>
