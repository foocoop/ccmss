<?php
require('footilities/funcionesHTML.php');
require('backend/mods.php');
require_once 'backend/script_creador_auto.php';


add_action('save_post', 'almacenar_db');
function almacenar_db($post_id){
  
  if (isset($_POST['file'])){
    $post_ID = $post_id;
    $post = get_post($post_id);
    $postType = $post->post_type;

    if( $postType == 'db' ) {
      update_post_meta($post_id, "file", $_POST["file"]);
      csv($post_id);
    }
  }
}

//add_action('new_to_publish_db', 'run_when_published');

// Custom password protected message / form
add_filter( 'the_password_form', 'custom_password_form' );
function custom_password_form() { global $post; $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID ); $o = '<form class="protected-post-form" action="' . get_option('siteurl') . '/wp-pass.php action=postpass" method="post">
' . __( "<h2>Enter Password</h2> 
<p>This proposal is password protected. To view it please enter your password below:</p>" ) . '
<label for="' . $label . '">' . __( "Password:" ) . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" /><input type="submit" class="submit" name="Submit" value="' . esc_attr__( "Submit" ) . '" /> </form> 
';
return $o;
}

// Filter to hide protected posts
function exclude_protected($where) {
  global $wpdb;
  return $where .= " AND {$wpdb->posts}.post_password = '' ";
}

// Decide where to display them
function exclude_protected_action($query) {
  if( !is_single() && !is_page() && !is_admin() ) {
    add_filter( 'posts_where', 'exclude_protected' );
  }
}

// Action to queue the filter at the right time
add_action('pre_get_posts', 'exclude_protected_action');



?>
