<?php


global $db_hostname,$db_username,$db_pwd;

function curPageURL() {
  $pageURL = 'http';
  if( isset($_SERVER["HTTPS"])) {
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
  }
  $pageURL .= "://";
  if ($_SERVER["SERVER_PORT"] != "80") {
    $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
  } else {
    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
  }
  
  return $pageURL;
}

$conexion=mysql_connect($db_hostname,$db_username,$db_pwd);
if (!$conexion)
die("Error de conexion al servidor ".mysql_error()); //El die debe reemplazarse por código de deployment
mysql_select_db("ccmss_data") or die (mysql_error());
$total_rows=mysql_num_rows(mysql_query("SELECT * from datos"));

$num_pags = floor( $total_rows / 100 );

$pagina = $pagina > $num_pags ? $num_pags : $pagina;
$pagina = $pagina < 1 ? 1 : $pagina;

$link = curPageURL();
$link = preg_replace('/\?.*/', '', $link );

$show_pags = 15;

$pag_offset = ceil($show_pags / 2);

$start_pag = $pagina - $pag_offset;
$start_pag < 1 ? $start_pag = 0 : 0;
$pagina + $pag_offset >= $num_pags ? $start_pag = $num_pags - $show_pags : 0;

$paginacion = "";
for($i=$start_pag;$i<$start_pag+$show_pags;$i++){
  $pag = ($i+1);
  $clase = $pag == $pagina ? 'class="actual"' : "";
  
  $paginacion .= '<li '.$clase.'><a href="'.$link.'?pagina='.$pag.'">'.$pag.'</a></li>';
}
$primera = $pagina > $pag_offset ?  '<li class="primera"><a href="'.$link.'?pagina=1">1</a></li>' : "";

if( $pagina < $num_pags - $pag_offset ) {
  $ultima = '<li class="ultima"><a href="'.$link.'?pagina='.( $num_pags ) .'">'. ($num_pags  ).'</a></li>';
}
$paginacion = $primera . $paginacion . $ultima;

$paginacion =  '<ul id="paginacion">' . $paginacion . '</ul>';

if($pagina > 1 ) {
  $offset = $pagina * 100;
}
else {
  $offset = 0;
}

$query = "SELECT * from datos LIMIT ".$offset.",100";
$result = mysql_query( $query );
$cols_query = mysql_query( "SELECT COLUMNS FROM datos" );

$rows = mysql_num_rows( $result );
$cols = 13;


$titulos_resultados = '';
$resultados = '';
$titulos_resultados .= '<ul class="titulos">';
$titulos_resultados .= '<li>-</li>';
for( $j = 1; $j <= $cols; $j++ ) {
  $nombre_col = mysql_field_name (  $result , $j );
  $titulos_resultados .= '<li>'.$nombre_col.'</li>';
}
$titulos_resultados .= '</ul>';

for( $i = 0; $i < $rows; $i++ ) {
  if( $i % 2 == 0 ) {
    $class_row = "even";
    $td_class = "_light";
  }
  else {
    $class_row = "odd";
    $td_class = "_dark";
  }

  $resultados .= '<ul class="'.$class_row.'">';
  $resultados .= '<li>'. (int)($offset +$i +1) . '</li>';
  for( $j = 1; $j <= $cols; $j++ ) {
    if( $j % 2 == 0 ) {
      $class_col = "even";
    }
    else {
      $class_col = "odd";
    }

    
    $resultados .= '<li class="'.$class_col.$td_class.'">'.mysql_result( $result, $i, $j ).'</li>';
  }
  $resultados .= '</ul>';
}
mysql_close($conexion);
?>
