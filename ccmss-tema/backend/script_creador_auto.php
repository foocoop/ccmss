<?php

require_once 'login.php';
include( __DIR__ . "/../../../CCMSS/wp-config.php" );

global $folder, $host;
$folder = '/var/www/html/';
$host='http://foocoop.mx/';

/*
$args=array('numberposts'=>1,'orderby'=>'post_date','order'=>'ASC','post_type'=>'db');

$Q = new WP_Query($args);
while($Q->have_posts()):$Q->the_post();
*/

$file = "";

$nombre_tabla ="";
$path ="";
function get_post_data($post_id) {

  global $file;
  global $path;
  global $nombre_tabla;
  global $folder, $host;
  
  $post = get_post($post_id);
  $custom = get_post_custom($post->ID);
  $nombre = apply_filters('the_title',$post->post_title );
  $nombre = preg_replace('/\s+/', '_',$nombre);
  $file = $custom["file"][0];
  /*
  endwhile;
   */

  $file = preg_replace('/\s+/', '_',$file);
  $file = str_replace($host,$folder,$file);
  $path=$file;
  $file = substr($file, 0, (strlen ($file)) - (strlen (strrchr($file,'.'))));

  $nombre_tabla ="datos_".$nombre;

}

$conexion = 0;

function csv($post_id)
{
  global $conexion;
  get_post_data($post_id);
  connect();
  crear_db();
  seleccionar_db();
  crear_datos();
  llenar_datos();
  mysql_close($conexion);
  echo "El script se automatizó con éxito.";
}

function connect()
{
  global $db_hostname,$db_username,$db_pwd;
  
  global $conexion;

  $conexion = mysql_connect($db_hostname,$db_username,$db_pwd);
  if (!$conexion)
  echo("Error de conexion al servidor ".mysql_error()); //El echo debe reemplazarse por código de deployment
  mysql_query("SET NAMES 'utf8'");
}

function crear_db()
{
  global $db_database;
  
  $query="CREATE DATABASE IF NOT EXISTS ".$db_database;
  $result = mysql_query($query);
  if(!$result)
  echo("Error creando la DB ".mysql_error());
}

function seleccionar_db()
{
  global $db_database;
  mysql_select_db($db_database)
  or die("Error de seleccion de DB ".mysql_error());
}

function crear_estados()
{
  $query="CREATE TABLE IF NOT EXISTS estados
	(
		indice int NOT NULL AUTO_INCREMENT,
		estado varchar(100),
		PRIMARY KEY (indice)
        )
        CHARACTER SET utf8 COLLATE utf8_spanish_ci";
  
  $result=mysql_query($query);
  if(!$result)
  echo("Error de conexion a la DB estados ".mysql_error());
}

function llenar_estados()
{
  global $estados;
  foreach ($estados as $estado) {
    $query = "INSERT INTO estados (estado) VALUES ('".$estado."')";
    $result=mysql_query($query);
    if(!$result)
    echo("Error de insercion en la DB estados ".mysql_error());
  }
}

function crear_datos()
{
  global $nombre_tabla;

  $query = "DROP TABLE ".$nombre_tabla;
  
  $result=mysql_query($query);
  if(!$result)
  echo("Error de borrado de tabla datos: ".mysql_error());
  $query="CREATE TABLE ".$nombre_tabla."
	(
		indice int NOT NULL AUTO_INCREMENT,
		programa text,
		cve_estado int,
                estado varchar(100),
		cve_municipio int,
		municipio varchar(255),
                solicitante text,
		tipo_beneficiario varchar(255),
		predio text,
		concepto1 varchar(255),
		concepto2 varchar(255),".
		/*superficie float(8,2),
		unidad varchar(100),*/
		"monto double,
                anho int,
                PRIMARY KEY (indice)
      )CHARACTER SET utf8";

  $result=mysql_query($query);
  if(!$result)
  die("Error de conexion a la DB datos ".mysql_error());
}

function llenar_datos()
{
  global $nombre_tabla,$path;
  
  $query ="
    LOAD DATA LOCAL INFILE '".$path."'
    INTO TABLE ".$nombre_tabla."
    CHARACTER SET utf8
    FIELDS TERMINATED BY '|'
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES".
    //(programa, cve_estado, estado, cve_municipio, municipio, solicitante, tipo_beneficiario, predio, concepto1, concepto2, superficie, unidad, monto, anho)
    "(programa, cve_estado, estado, cve_municipio, municipio, solicitante, tipo_beneficiario, predio, concepto1, concepto2, monto, anho)
    ";
  
  $result=mysql_query($query);
  if(!$result)
  echo("Error de insercion en la DB datos datos ".mysql_error());
}

/*
connect();
crear_db();
seleccionar_db();
//crear_estados();
//llenar_estados();
crear_datos();
llenar_datos();

mysql_close($conexion);

echo("Chidou. Winners don't do drugs.");
 */
?>
