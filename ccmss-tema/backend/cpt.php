<?php


function remove_default_post_type(){

	remove_menu_page( 'edit.php' );                   //Posts
  	remove_menu_page( 'upload.php' );                 //Media
  	//remove_menu_page( 'edit.php?post_type=page' );    //Pages
  	remove_menu_page( 'edit-comments.php' );          //Comments
}
add_action('admin_menu','remove_default_post_type');
/*
add_action('init','cpt');
function cpt(){
	
	register_post_type('publicacion',
    array(
      'labels' => array(
        'name' => __( 'Publicaciones' ),
        'singular_name' => __( 'Publicacion' )
      ),
      'public'=> true,
      'hierarchical'=>true,
      'supports'=> array('title','editor','thumbnail','excerpt','custom-fields')
      )
    );
  
}


function add_url_meta_box(){
  add_meta_box(
    'url',//id
    'URL',//title
    'show_url_meta_box',//callback
    'publicacion',//post_type donde aparecerá
    'normal',//context
    'high'//prioridad
    );
}
add_action('add_meta_boxes','add_url_meta_box');

$prefix='custom_';
$custom_meta_fields=array(
  array(
    'label'=>'Título',
    'desc'=>'Título',
    'id'=>$prefix.'titulo',
    'type'=>'text'
    ),
  array(
    'label'=>'Url',
    'desc'=>'Url de link',
    'id'=>$prefix.'url',
    'type'=>'text'
    )
  );

function show_url_meta_box(){
  global $custom_meta_fields, $post;
  // Use nonce for verification
  echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
  //Mostrar el campo
  echo '<table class="form-table">';
    foreach ($custom_meta_fields as $field) {
        // get value of this field if it exists for this post
        $meta = get_post_meta($post->ID, $field['id'], true);
        // begin a table row with
        echo '<tr>
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
                <td>';
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
                  <br /><span class="description">'.$field['desc'].'</span>';
        echo '</td></tr>';
    } // end foreach
    echo '</table>'; // end table
}

// Save the Data
function save_custom_meta($post_id) {
    global $custom_meta_fields;
     
    // verify nonce
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
        return $post_id;
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    // check permissions
    if ('page' == $_POST['publicacion']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }
     
    // loop through fields and save the data
    foreach ($custom_meta_fields as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    } // end foreach
}
add_action('save_post', 'save_custom_meta');




*/


?>
