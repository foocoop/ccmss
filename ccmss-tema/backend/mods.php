<?php

function remove_default_post_type(){

	remove_menu_page( 'edit.php' );                   //Posts
  	remove_menu_page( 'upload.php' );                 //Media
  	//remove_menu_page( 'edit.php?post_type=page' );    //Pages
  	remove_menu_page( 'edit-comments.php' );          //Comments
}
add_action('admin_menu','remove_default_post_type');

?>
