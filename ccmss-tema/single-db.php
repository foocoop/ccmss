<?php
/**
 * Single
 *
 * Loop container for single post content
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 4.0
 */

get_header(); ?>


<div class="large-9 columns" role="main">
<?php 
$temp = $wp_query;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$post_per_page = 100; // -1 shows all posts
$args=array(
  'post_type' => 'db',
  'orderby' => 'date',
  'order' => 'ASC',
  'paged' => $paged,
  'posts_per_page' => $post_per_page
);
$wp_query = new WP_Query($args); 

if( have_posts() ) : while ($wp_query->have_posts()) : $wp_query->the_post();
$custom = get_post_custom($post->ID);
$file = $custom["file"][0];





?>

    <!-- Main Content -->
    <li>
      <a href="<?php if ($file) { echo $file; } else { echo "#"; } ?>"><?php the_title(); ?></a>
        </li>



            <?php endwhile; else: ?>
      <?php endif; wp_reset_query(); $wp_query = $temp; the_post(); ?>

      

    </div>
    <!-- End Main Content -->

    
<?php get_footer(); ?>