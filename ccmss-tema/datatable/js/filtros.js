(function($) {


    var path = '';
    var oTable; 
    var minMonto=0, maxMonto=350000;

    var amountColumn = 6; 

    function getIsSelectFilter( i ) {
        
        if( i < amountColumn )
            return true;
        
        return false;
    }
    
    function getIsAmountFilter( i ) {
        if( i == amountColumn )
            return true;

        return false;
    }
    

    function fnGetSelected( oTableLocal )
    {
        return oTableLocal.$('tr.row_selected');
    }


    function realizarOperaciones() {
        var suma_final = 0;
        var promedio_final = 0;

        var trs = $('#operaciones tbody tr').not('#suma, #promedio');

        var activeTrs = 0;

	trs.each(function(){
	    if( !$(this).hasClass('disabled') ) {
		var tr_tds = $(this).find('td'); 
                var str = tr_tds.eq( tr_tds.length - 2 ).html();
                while(str.indexOf(',')!=-1)str= str.replace(',','');
                var monto_td = parseFloat( str );

		suma_final += monto_td;
		activeTrs++;
	    }

	});
	
        promedio_final = activeTrs > 0 ? suma_final / activeTrs : 0;
	
        var suma_tds = $('#suma td');
        var promedio_tds = $('#promedio td');
        suma_tds.eq( suma_tds.length - 2 ).html( suma_final.toFixed(2) );
        promedio_tds.eq( promedio_tds.length - 2 ).html( promedio_final.toFixed(2) );
    }
    
    $(document).ready(function() {
        
        
	path = $('#stylesheet_url').html() + '/';
	
        
        
        /* Add a select menu for each TH element in the table footer */
	
        $('#pie_tabla th').each(function(i){
            $(this).append($('<div>').addClass('customSearch hidden').html(''));
	});
 
        oTable = $('#example').dataTable( {
            "bProcessing": true,
            "bServerSide": true,
	    "bFilter": true,
            "sAjaxSource": path+"scripts/server_processing.php",
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "sScrollY": "200",
            "bScrollCollapse": true,
	    //"aoColumnDefs": [{"bVisible":false,"aTargets":[8]}],
	    "fnServerParams": function (aoData) { /* pass custom variables to server */

                $('#pie_tabla th').each(function(i){
                    var cS = $(this).find('.customSearch').html();
		    
                    console.error("arreglar ` en customSearch", 'customSearch_'+i, cS);
                    aoData.push({ "name": 'customSearch_'+i, "value": cS });
                });
		
                var r = new RegExp("\\D");
                var minMontoVal, maxMontoVal;
                minMontoVal = $('#min').val();
		maxMontoVal = $('#max').val();

                var resetMontos = false;
		
		if( minMontoVal != "" && maxMontoVal != "" ) {
		    if( ! r.test( minMontoVal ) && ! r.test( maxMontoVal ) ) {
			if( parseInt( maxMontoVal ) >= parseInt( minMontoVal )  ) {			
			    aoData.push({ "name": 'minMonto', "value": minMontoVal });
			    aoData.push({ "name": 'maxMonto', "value": maxMontoVal });
			}
                    }
                }

                var minAnhoVal, maxAnhoVal;
                minAnhoVal = $('#anho_min').val();
                maxAnhoVal = $('#anho_max').val();

                var resetAnhos = false;
                
                if( minAnhoVal != "" && maxAnhoVal != "" ) {
                    if( ! r.test( minAnhoVal ) && ! r.test( maxAnhoVal ) ) {
                        if( parseInt( maxAnhoVal ) >= parseInt( minAnhoVal )  ) {                     
                            aoData.push({ "name": 'minAnho', "value": minAnhoVal });
                            aoData.push({ "name": 'maxAnho', "value": maxAnhoVal });
                        }
                    }
                }

            },
            "fnDrawCallback": function( oSettings ) {
                $('#example tbody tr').click( function() {                    
                    
                    var tr = $(this);

		    tr.toggleClass('row_selected');

		    if(tr.hasClass('row_selected')) {
			var newtr = tr.clone()
				.removeClass('even')
				.removeClass('odd')
				.removeClass('row_selected');

			numTrs = $('#operaciones tr').length;

			newtr.addClass( numTrs % 2 ? 'odd' : 'even' );
			newtr.prepend('<td class="removetr"><a href="#">Eliminar</a></td>');
			newtr.prependTo('#operaciones tbody');

			
			realizarOperaciones();

			newtr.find('.removetr').click(function(e){
			    $(this).parent().remove();
                            realizarOperaciones();
			    e.preventDefault();
			});
                        newtr.click(function(e){
			    newtr.toggleClass('disabled');
                            realizarOperaciones();
                            e.preventDefault();
                        });
                        
                    }

                });
		
                
            }
            
        });
        
        var r = new RegExp("\\D");

        
        $('#min').keyup( function() {
            var val = $(this).val();
            if(!r.test(val)) {
                oTable.fnDraw();
            }
        });
        
        $('#max').keyup( function() {
            var val = $(this).val();
            if(!r.test(val)) {
                oTable.fnDraw();
            }
        });


        for(var i=1; i<=amountColumn; i++) {
	    
            if( getIsSelectFilter( i ) ) {
                oTable.makeDropdown(i);
            }
            else if( getIsAmountFilter( i ) ) {
                $('.rangoinput').detach().appendTo( $('#pie_tabla th').eq(i) );
            }
	    else {
	    }

        }

    });




    // setear la información en customSearch:
    function filtrar( i ) {
	
        //$('tfoot th').each(function(i) {
        var th = $('#pie_tabla th').eq(i);
        
        var searchStr = "";
        //var th = $(this);
        var sels = th.find('select');
	sels.each( function(j) {
            var val = $(this).val();
	    if( val != "" ) {
		searchStr += val;
		if( j < sels.length - 1) {
		    searchStr += ",";
		}
	    }
        });

        // escribir valores en div.customsearch:
	th.find('.customSearch').html( searchStr );

	oTable.fnFilter( searchStr, i );
	//});


        
    }



    function añadirFiltro( filtrodiv ) {                        
        var th = filtrodiv.parent();
        th.append( filtrodiv.clone() );
        setupSelWdgts( th.index() );
    }

    function quitarFiltro( filtro ) {                        
        var i = filtro.parent().index();
	filtro.remove();
	setupSelWdgts( i );
    }






    function setupSelWdgts(i){

        var th = $('#pie_tabla th').eq(i);

        var filtrodiv = th.find('.filterselect').last();
        var btnadd = filtrodiv.find('.btnaddfilter');
        var btnrmv = filtrodiv.find('.btnrmvfilter');
	
        btnadd.unbind('click');
        btnadd.click( function(e) {                        
            añadirFiltro( filtrodiv );
	    e.preventDefault();
        });
	btnrmv.unbind('click');
        btnrmv.click( function(e) {                        
            var filtro = btnrmv.parent().parent();
            console.log("siblig:", filtro.siblings().length );
	    if(filtro.siblings().length>1){
		quitarFiltro( filtro );
	    }
	    filtrar( i );
	    e.preventDefault();
        });

        // si hay más de un filterselect
        var filtersels = th.find('.filterselect');
	if( filtersels.length > 1 )
	{
            filtersels.each( function() {
		$(this).find('.btnrmvfilter').addClass('enabled');
	    });            
        }
	else
        {
            filtersels.first().find('.btnrmvfilter').removeClass('enabled');
        }            
        
        var thW = th.width();
        var filterselects = th.find('.filterselect');
        var selobj = filterselects.last();

	selobj.width( thW );
        
        selobj.change( function () {
            filtrar( i );
        });

        
    }

    
    $.fn.dataTableExt.oApi.makeDropdown = function ( oSettings, iColumn ) {
        // check that we have a column id
        if ( typeof iColumn == "undefined" ) return new Array();

        // list of rows which we're going to loop through
        var aiRows;
        var php = path+"scripts/dropdown_query.php";
        var asResultData = new Array();
	// use only filtered rows
        
        $.ajax({ url: php, type: 'POST', dataType: 'json', data: { colIndex: iColumn },
                 success: function(data) {

                     var sel = fnCreateSelect( data );
                     var btnadd = $('<a>').attr('href','#').html( $('<input  value="+" type="button">').attr('class','btnaddfilter boton').html('+') );
                     var btnrmv = $('<a>').attr('href','#').html( $('<input  value="-" type="button">').attr('class','btnrmvfilter boton').html('-') );

                     sel.append( btnadd );
                     sel.append( btnrmv );

		     var th = $('#pie_tabla th').eq(iColumn);
		     
		     th.append( sel );

		     setupSelWdgts( iColumn );
   		     
		 }
	       });
        return asResultData;
    }}(jQuery));





function fnCreateSelect( aData )
{
    
    var r='<select><option value=""></option>', i, iLen=aData.length;
    for ( i=0 ; i<iLen ; i++ )
    {
        r += '<option value="'+aData[i]+'">'+aData[i]+'</option>';
    }
    var sel =  $('<div>').attr('class','select').html(r+'</select>');
    
    var div = $('<div>').attr('class','filterselect').append( sel );
	
    return div;
}
