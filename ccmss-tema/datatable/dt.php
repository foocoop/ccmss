<?php
// datatable

$titulos=array(
  'Programa', 'Estado', 'Municipio',
  'Tipo de beneficiario',/* 'Concepto 1',*/
  'Concepto', 'Monto', 'Año'
);

$titulosOperaciones=array(
  'Eliminar','Índice','Programa', 'Estado', 'Municipio',
  'Tipo de beneficiario',/* 'Concepto 1',*/
  'Concepto', 'Monto', 'Año'
);

  
function tags($titulos,$skipfirst=true,$type="th"){

  //$fill = 8;
  $fill = 7;
  if( $skipfirst ) {
    echo '<'.$type.' width="5.6%"></'.$type.'>';
  }
  if( $titulos ) {
    foreach($titulos as $t) {
      echo '<'.$type.' width="11.8%">'.$t.'</'.$type.'>';
      $fill--;
    }
  }

  for($i=0;$i<$fill;$i++)
    echo '<'.$type.' width="11.8%"></'.$type.'>';

}

?>
		<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/datatable/media/js/jquery.js"></script>
                <script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/datatable/media/js/jquery.dataTables.js"></script>
                <script type="text/javascript" charset="utf-8" src="<?php echo get_stylesheet_directory_uri(); ?>/datatable/js/filtros.js"></script>
                <script type="text/javascript" charset="utf-8" src="<?php echo get_stylesheet_directory_uri(); ?>/datatable/js/exportar.js"></script>
                
 
	  <div id="container">
            <div id="rangos_montos" class="rangos">
              
              <input class="rangoinput" id="min" value="0">
              <input class="rangoinput" id="max" value="100000000">
            </div>
            <!--
            <div id="rangos_anhos" class="rangos">
              
               <input class="rangoinput" id="anho_min" value="2010">
               <input class="rangoinput" id="anho_max" value="2099">
            </div>
            -->
            
			<div id="table_holder">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
	  <tr>

<?php
                   tags($titulos,true);
?>

                   
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="5" class="dataTables_empty">Loading data from server</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
<?php

?>
                </tr>
	</tfoot>        
</table>
<table id="pie_tabla">
<tr>
<?php
                   tags(array());
?>
</tr>
</table>

<a id="exportar_consulta" href="#">Exportar Consulta</a>

<!-- <div id="operaciones"> -->
  <table cellpadding="0" cellspacing="0" border="0" class="display" id="operaciones">
    <thead>
      <tr>
<?php
tags($titulosOperaciones,false);
?>
</tr>
    </thead>
    <tbody>

<tr id="suma" class=""><?php tags(array('','','','','','Suma:','',''),true,'td'); ?></tr>
<tr id="promedio" class=""><?php tags(array('','','','','','Promedio:','',''),true,'td'); ?></tr>
    </tbody>
    <tfoot>
</tfoot>
    
  </table>


  <a id="exportar_resultados" href="#">Exportar Resultados</a>

  <!-- </div> -->


			</div>
			
		</div>
                

                
