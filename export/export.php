<?php




global $Sql,$sTable,$sIndexColumn;

require_once("../datatable/globales/auth.php");
require_once("../datatable/globales/campos.php");

/* 
 * Local functions
 */
function fatal_error ( $sErrorMessage = '' )
{
  header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
  die( $sErrorMessage );
}

/* 
 * MySQL connection
 */
if ( ! $Sql['link'] = mysql_pconnect( $Sql['server'], $Sql['user'], $Sql['password']  ) )
{
  fatal_error( 'Could not open connection to server' );
}


mysql_set_charset('utf8');


if ( ! mysql_select_db( $Sql['db'], $Sql['link'] ) )
{
  fatal_error( 'Could not select database ' );
}


$sQuery = "SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
                FROM   $sTable
                WHERE (programa='Dendroenergía' )
                ";
$rResult = mysql_query( $sQuery, $Sql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() );

$output = array();

while ( $aRow = mysql_fetch_array( $rResult ) )
{
  $row = array();
  for ( $i=0 ; $i<count($aColumns) ; $i++ )
  {
    $row[] = str_replace( "?", " ", $aRow[ $aColumns[$i] ] );
  }
  $output[] = $row;
}

//var_dump($output);


require('exportXLS.php');


exportar( $output );



/*







error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

  require_once dirname(__FILE__) . '/../xls2php/PHPExcel/Classes/PHPExcel.php';
  // Create new PHPExcel object
  echo date('H:i:s') , " Create new PHPExcel object" , EOL;
  $objPHPExcel = new PHPExcel();

  // Set document properties
  echo date('H:i:s') , " Set properties" , EOL;
$objPHPExcel->getProperties()->setCreator("CCMSS")
            ->setLastModifiedBy("FooCoop")
            ->setTitle("Resultados de búsqueda")
            ->setSubject("")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");


  // Create a first sheet
  echo date('H:i:s') , " Add data" , EOL;
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('A1', "Índice");
$objPHPExcel->getActiveSheet()->setCellValue('B1', "Programa");
$objPHPExcel->getActiveSheet()->setCellValue('C1', "Estado");
$objPHPExcel->getActiveSheet()->setCellValue('D1', "Municipio");
$objPHPExcel->getActiveSheet()->setCellValue('E1', "Tipo de Beneficiario");
$objPHPExcel->getActiveSheet()->setCellValue('F1', "Concepto 1");
$objPHPExcel->getActiveSheet()->setCellValue('G1', "Concepto 2");
$objPHPExcel->getActiveSheet()->setCellValue('H1', "Monto");
$objPHPExcel->getActiveSheet()->setCellValue('I1', "Año");



// Add data
$i=2;

foreach($output as $row) {
  $j=0;
  foreach($row as $cell) {
    $objPHPExcel->getActiveSheet()->setCellValue(chr(65+$j) . $i, $cell);
    $j++;
  }
  $i++;
}


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Save Excel 95 file
echo date('H:i:s') , " Write to Excel5 format" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save(str_replace('.php', '.xls', __FILE__));
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


// Echo memory peak usage
echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

// Echo done
echo date('H:i:s') , " Done writing file" , EOL;
echo 'File has been created in ' , getcwd() , EOL;

*/
?>