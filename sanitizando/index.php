<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

  <head>
    <title>An XHTML 1.0 Strict standard template</title>
    <meta http-equiv="content-type" 
          content="text/html;charset=utf-8" />
  </head>

  <?php

  global $Sql,$errorFile;

  require_once("../globales/auth.php");
  require_once("../scripts/errorLog.php");
  
  $conexion = @mysql_connect($Sql['server'],$Sql['user'] ,$Sql['password'] );
  if (!$conexion)
    log_error("Error de conexion al servidor ".mysql_error());
  mysql_query("SET NAMES 'utf8'");
  if(!mysql_select_db($Sql['db'] ))
    log_error("Error de seleccion de DB ".mysql_error());

  $nombre_tabla="test_sanitize";
  $path="temp.csv";
  $query = "DROP TABLE ".$nombre_tabla;
  $result=mysql_query($query);
  if(!$result)
    log_error("Error de borrado de tabla datos: ".mysql_error());

  $query="CREATE TABLE ".$nombre_tabla."
        (
                indice int NOT NULL AUTO_INCREMENT,
                programa text,
                cve_estado int,
                estado varchar(100),
                cve_municipio int,
                municipio varchar(255),
                solicitante text,
                tipo_beneficiario varchar(255),
                predio text,
                concepto1 varchar(255),
                concepto2 varchar(255),".
         /*superficie float(8,2),
         unidad varchar(100),*/
         "monto double,
                anho int,
                PRIMARY KEY (indice)
      )CHARACTER SET utf8";

  $result=mysql_query($query);
  if(!$result)
    log_error("Error de conexion a la DB datos ".mysql_error());
  
  $row = 0;
  if (($handle = fopen("test.csv", "r")) !== FALSE) {
    if(($handle2=fopen("temp.csv","w"))!==FALSE) {
      //$rowsArr = array();
      while (($data = fgetcsv($handle, 1000, "|")) !== FALSE) {
        $fieldArr = array();
        $num = count($data);
        //if($row>0)
        {
          $valueStr="";
          for ($c=0; $c < $num; $c++) {
            if(($c!=10)&&($c!=11))
            {
              
              $field = filter_var($data[$c],FILTER_SANITIZE_SPECIAL_CHARS);
              //$field=str_replace('"',"",$data[$c]);
              array_push( $fieldArr, $field );
              //if( $field == "" ) { $field = NULL; }
              /*$valueStr .= "'" . $field . "'";
              if($c<$num-1)
              $valueStr.=", ";
               */
            }
            
          }
          fputcsv($handle2,$fieldArr,"|");
          //array_push($fieldArr, $valueStr);
        }
        $row++;
      }
    }


    
    fclose($handle);
    fclose($handle2);

    /*

    $colStr = "programa, cve_estado, estado, cve_municipio, municipio, ";
    $colStr .= "solicitante, tipo_beneficiario, predio, concepto1, concepto2, monto, anho";

    $query = "INSERT INTO ".$nombre_tabla." (".$colStr.") VALUES (";

    for($i=0; $i < count( $fieldArr ); $i++ ){

      $valueStr = $fieldArr[$i];

      $queryI = $query . $valueStr . ")";

      $result=mysql_query($queryI);
      
      if(!$result) {
        echo("Error de insercion en la DB datos datos ".mysql_error());
      }
      
    }
     */


    $query ="
    LOAD DATA LOCAL INFILE '".$path."'
    INTO TABLE ".$nombre_tabla."
    CHARACTER SET utf8
    FIELDS TERMINATED BY '|'
    ENCLOSED BY '\"'
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES".
            //(programa, cve_estado, estado, cve_municipio, municipio, solicitante, tipo_beneficiario, predio, concepto1, concepto2, superficie, unidad, monto, anho)
            "(programa, cve_estado, estado, cve_municipio, municipio, solicitante, tipo_beneficiario, predio, concepto1, concepto2, monto, anho)
    ";
    
    $result=mysql_query($query);
    if(!$result)
      log_error("Error de insercion en la DB datos datos ".mysql_error());

  }
  
  
  mysql_close($conexion);

  /*function log_error($texto)
  {
    date_default_timezone_set('America/Mexico_City');
    $date = date('d/m/Y h:i:s a', time());
    file_put_contents($errorFile, $date." ".$texto,FILE_APPEND);
    echo("Ha ocurrido un error en la página. Por favor póngase en contacto con el administrador.");
    die();
  }*/

  ?>