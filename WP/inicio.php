<?php
	/*
	Template Name: Incio
	*/
	require_once 'login.php';

	if (isset($_GET["pagina"])) {
  	$pagina = $_GET["pagina"];
	  
	} else {
  		$pagina = 1;
	}
	get_header();
	$publicaciones="";
	$args=array('post_type'=>'publicacion','posts_per_page'=>-1);
	$Q = new WP_Query($args);
	while($Q->have_posts()) : $Q->the_post();
		$P = foo_post();
		$contenido = foo_div("","titulo",foo_h($P['ttl'],3));
		$contenido .= foo_div("","contenido","<p>".$P['cnt']."</p>");
		$contenido .= foo_div("","imagen",foo_img( foo_thumb( foo_featImg("medium"),120,120)));
		$publicaciones .= foo_div("","publicacion item",$contenido);
	endwhile;
	$publicaciones=foo_div("publicaciones","",$publicaciones);
	echo $publicaciones;
	get_footer();
?>