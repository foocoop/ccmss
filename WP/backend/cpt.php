<?php
add_action('init','cpt');
add_action('admin_menu','remove_default_post_type');

function cpt(){
	
	register_post_type('publicacion',
    array(
      'labels' => array(
        'name' => __( 'Publicaciones' ),
        'singular_name' => __( 'Publicacion' )
      ),
      'public'=> true,
      'hierarchical'=>true,
      'supports'=> array('title','editor','thumbnail')
      )
    );

    register_post_type('csv',
    array(
      'labels' => array(
        'name' => __( 'Csvs' ),
        'singular_name' => __( 'Csv' )
      ),
      'public'=> true,
      'hierarchical'=>true,
      'supports'=> array('title','editor','thumbnail')
      )
    );
}

function remove_default_post_type(){

	remove_menu_page( 'edit.php' );                   //Posts
  	remove_menu_page( 'upload.php' );                 //Media
  	//remove_menu_page( 'edit.php?post_type=page' );    //Pages
  	remove_menu_page( 'edit-comments.php' );          //Comments
}
?>
