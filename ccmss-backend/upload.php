<?php
/*
Plugin Name: Backend para CCMSS
Author: foocoop
*/









class CPT_File_Upload {


  public function __construct() {


    
    add_action( 'init', array( &$this, 'init' ) );

    if ( is_admin() ) {
      add_action( 'admin_init', array( &$this, 'admin_init' ) );
    }
  }


  /** Frontend methods ******************************************************/


  /**
   * Register the custom post type
   */
  

  function registrar_db() {
    $labels = array(
      'name' => _x('Db', 'post type general name'),
      'singular_name' => _x('Db', 'post type singular name'),
      'add_new' => _x('Add New', 'Db'),
      'add_new_item' => __('Add New Db'),
      'edit_item' => __('Edit Db'),
      'new_item' => __('New Db'),
      'view_item' => __('View Db'),
      'search_items' => __('Search Dbs'),
      'not_found' =>  __('Nothing found'),
      'not_found_in_trash' => __('Nothing found in Trash'),
      'parent_item_colon' => ''
    );

    $args = array(
      'labels' => $labels,
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'menu_position' => null,
      'supports' => array('title','editor','thumbnail'),
      'rewrite' => array('slug' => 'db', 'with_front' => FALSE)
    );
    register_post_type( 'db', $args );
  }

  // post type "publicacion"

  function registrar_publicacion(){
    
    register_post_type('publicacion',
                       array(
        'labels' => array(
          'name' => __( 'Publicaciones' ),
          'singular_name' => __( 'Publicacion' )
        ),
        'public'=> true,
        'hierarchical'=>true,
        'supports'=> array('title','editor','thumbnail','excerpt','custom-fields')
      ));


    
    function add_url_meta_box(){
      add_meta_box(
        'url',//id
        'URL',//title
        'show_url_meta_box',//callback
        'publicacion',//post_type donde aparecerá
        'normal',//context
        'high'//prioridad
      );
    }
    add_action('add_meta_boxes','add_url_meta_box');

    
    $prefix='custom_';
    $custom_meta_fields=array(
      array(
        'label'=>'Título',
        'desc'=>'Título',
        'id'=>$prefix.'titulo',
        'type'=>'text'
      ),
      array(
        'label'=>'Url',
        'desc'=>'Url de link',
        'id'=>$prefix.'url',
        'type'=>'text'
      )
    );

    function show_url_meta_box(){
      global $custom_meta_fields, $post;
      // Use nonce for verification
      echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
      //Mostrar el campo

      if( $custom_meta_fields ) {

        echo '<table class="form-table">';

        foreach ($custom_meta_fields as $field) {
          // get value of this field if it exists for this post
          $meta = get_post_meta($post->ID, $field['id'], true);
          // begin a table row with
          echo '<tr>
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
                <td>';
          /*switch($field['type']) {
          // text
          case 'text':
          echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
          <br /><span class="description">'.$field['desc'].'</span>';
          break;
          } //end switch*/
          echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
                  <br /><span class="description">'.$field['desc'].'</span>';
          echo '</td></tr>';
        } // end foreach
        echo '</table>'; // end table

      }
    }

    // Save the Data
    function save_custom_meta($post_id) {
      global $custom_meta_fields;
      
      // verify nonce
      if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
      return $post_id;
      // check autosave
      if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
      return $post_id;
      // check permissions
      if ('page' == $_POST['publicacion']) {
        if (!current_user_can('edit_page', $post_id))
        return $post_id;
      } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
      }
      
      // loop through fields and save the data
      foreach ($custom_meta_fields as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
          update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
          delete_post_meta($post_id, $field['id'], $old);
        }
      } // end foreach
    }
    add_action('save_post', 'save_custom_meta');
    
  }

  // termina "publicacion"




  public function init() {


    $this->registrar_db();
    $this->registrar_publicacion();
    
  }


  
  /** Admin methods ******************************************************/


  /**
   * Initialize the admin, adding actions to properly display and handle 
   * the Db custom post type add/edit page
   */
  public function admin_init() {
    
    function save_details(){
      global $post;

      if( $post->post_type == "db" ) {
        update_post_meta($post->ID, "file", $_POST["file"]);
      }

    }
    
    function pdf_meta() {
      global $post;
      $custom = get_post_custom($post->ID);
      $file = $custom["file"][0];
      echo '<input type="hidden" name="mytheme_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
      
?>
<p><label>Uploaded File:</label><br />
  <input type="text" name="file" id="file" value="<?php echo $file; ?>" style="width:258px">
  <input type="button" name="pdf_button" id="pdf_button" value="Browse" style="width:258px"></p> 
  <?php
  }
  add_meta_box("pdf-meta", "Upload", "pdf_meta", "db", "side", "low");
  add_action('save_post', 'save_details');


  function my_admin_scripts() {
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    $path = "/ccmss-backend/js";
    $plugrl = plugins_url( $path );
    $scriptpath = $plugrl . '/upload.js';
    wp_register_script('file_upload', $scriptpath, array('jquery','media-upload','thickbox'));
    wp_enqueue_script('file_upload');
  } 
  function my_admin_styles() {
    wp_enqueue_style('thickbox');
  }
  add_action('admin_print_scripts', 'my_admin_scripts');
  add_action('admin_print_styles', 'my_admin_styles');

  }

  }

  // finally instantiate our plugin class and add it to the set of globals
  $GLOBALS['custom_post_type_file_upload'] = new CPT_File_Upload();