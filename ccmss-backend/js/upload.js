jQuery(document).ready(function() {

    jQuery('#pdf_button').click(function() {

        window.send_to_editor = function(html) {
            imgurl = jQuery(html).attr('src') || jQuery(html).find('img').attr('src') || jQuery(html).attr('href');
            jQuery('#file').val(imgurl);
            tb_remove();
        }

        tb_show('', 'media-upload.php?TB_iframe=true');
        return false;
    });

});
