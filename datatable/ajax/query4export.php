<?php

global $Sql,$sTable,$sIndexColumn;

require_once("../../globales/auth.php");
require_once("../../globales/campos.php");
require_once("exportXLS.php");

if( isset( $_POST['valores'] ) ) {
  $valores = $_POST['valores'];

  $numValores = count($valores);
  
  $qWhere = "WHERE ";
  foreach( $valores as $i => $valor ) {
    if($valor!="") {
      $valorexplode = explode(",", $valor);
      $j = 0;
      $qWhere .= " (";
      //if( $i < 6 ) {
        if( $i < 5 ) {
        foreach( $valorexplode as $val ) {
          $qWhere .= $aColumns[$i+1] . "='" . $val . "' ";
          if( $j < count( $valorexplode ) - 1 ) {
            $qWhere .= "OR ";
          }
          $j++;
        }
      }
      else {
        // integrar monto
        $qWhere .= $aColumns[$i+1] . " BETWEEN " . $valorexplode[0] . " AND " . $valorexplode[1] . " ";
      }
      $qWhere .= " )";
      if( $i < $numValores - 1 ) {
        $qWhere .= " AND ";
      }
    }
  }

  if ( ! $Sql['link'] = mysql_connect( $Sql['server'], $Sql['user'], $Sql['password']  ) )
  {
    fatal_error( 'Could not open connection to server' );
  }

  mysql_set_charset('utf8');

  if ( ! mysql_select_db( $Sql['db'], $Sql['link'] ) )
  {
    fatal_error( 'Could not select database ' );
  }
  $query="SELECT * FROM ". $sTable." ".$qWhere;
  //$qWhere=WHERE  (municipio='Abasolo'  ) AND  (monto BETWEEN 0 AND 100000000  ) 
  $array =  mysql_query( $query, $Sql['link'] );


  $output = array();
  while ( $aRow = mysql_fetch_array( $array ) )
  {
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
      $row[] = str_replace( "?", " ", $aRow[ $aColumns[$i] ] );
    }
    $output[] = $row;
  }


  /*
  die($qWhere);
  $result=array();

  while($rows = mysql_fetch_array($array)) {
    
    array_push($result,$rows[0]);
  }
   */

  
  mysql_close($Sql['link']);

  $xlsUrl = exportar( $output );

  die( $xlsUrl );

}

?>
