<?php
// datatable

$titulos=array(
  'Programa', 'Estado', 'Municipio',
  'Tipo de beneficiario',/* 'Concepto 1',*/
  'Concepto', 'Monto', 'Año'
);

  
function tags($titulos,$type="th"){

  //$fill = 8;
  $fill = 7;
  echo '<'.$type.' width="5.6%"></'.$type.'>';
  if( $titulos ) {
    foreach($titulos as $t) {
      echo '<'.$type.' width="11.8%">'.$t.'</'.$type.'>';
      $fill--;
    }
  }

  for($i=0;$i<$fill;$i++)
    echo '<'.$type.' width="11.8%"></'.$type.'>';

}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/media/images/favicon.ico" />
		<title>DataTables example</title>
		<style type="text/css" title="currentStyle">
                 @import "media/css/demo_page.css";		 
                 @import "media/css/demo_table.css";
                 
                 @import "media/css/style.css";
		</style>
		<script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
                <script type="text/javascript" charset="utf-8" src="js/filtros.js"></script>
                <script type="text/javascript" charset="utf-8" src="js/exportar.js"></script>
                
 
	</head>
	<body>
	  <div id="container">

              <input class="rangoinput" id="min" value="0">
              <input class="rangoinput" id="max" value="100000000">

            
			<div id="table_holder">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
	  <tr>

<?php
                   tags($titulos);
?>

                   
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="5" class="dataTables_empty">Loading data from server</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
<?php

?>
                </tr>
	</tfoot>        
</table>
<table id="pie_tabla">
<tr>
<?php
                   tags(array());
?>
</tr>
</table>

<a id="exportar_consulta" href="#">Exportar Consulta</a>

<!-- <div id="operaciones"> -->
  <table cellpadding="0" cellspacing="0" border="0" class="display" id="operaciones">
    <thead>
      <tr>
<?php
                   tags($titulos);
?>
</tr>
    </thead>
    <tbody>

<tr id="suma" class=""><?php tags(array('','','','','','Suma:'),'td'); ?></tr>
<tr id="promedio" class=""><?php tags(array('','','','','','Promedio:'),'td'); ?></tr>
    </tbody>
    <tfoot>
</tfoot>
    
  </table>


  <a id="exportar_resultados" href="#">Exportar Resultados</a>

  <!-- </div> -->


			</div>
			
		</div>
                

                
	</body>


</html>
