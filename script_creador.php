<?php
require_once 'login.php';
/* $db_hostname="localhost";
$db_database="ccmss_data";
$db_username="foocoop";
$db_pwd="f00@My5ql!"; */

/**********************************************************/

$estados = array('Aguascalientes',
	         'Baja California',
	         'Baja California Sur',
	         'Campeche',
	         'Coahuila',
	         'Colima',
	         'Chiapas',
	         'Chihuahua',
	         'Distrito Federal',
	         'Durango',
	         'Guanajuato',
	         'Guerrero',
	         'Hidalgo',
	         'Jalisco',
	         'México',
	         'Michoacán',
	         'Morelos',
	         'Nayarit',
	         'Nuevo León',
	         'Oaxaca',
	         'Puebla',
	         'Querétaro',
	         'Quintana Roo',
	         'San Luis Potosí',
	         'Sinaloa','Sonora',
	         'Tabasco',
	         'Tamaulipas',
	         'Tlaxcala',
	         'Veracruz',
	         'Yucatán',
	         'Zacatecas',
	         'Nacional');

$conexion = 0;

function connect()
{
  global $db_hostname,$db_username,$db_pwd;
  
  global $conexion;

  $conexion = mysql_connect($db_hostname,$db_username,$db_pwd);
  if (!$conexion)
  die("Error de conexion al servidor ".mysql_error()); //El die debe reemplazarse por código de deployment
  mysql_query("SET NAMES 'utf8'");
}

function crear_db()
{
  global $db_database;
  
  $query="CREATE DATABASE IF NOT EXISTS ".$db_database;
  $result = mysql_query($query);
  if(!$result)
  die("Error creando la DB ".mysql_error());
}

function seleccionar_db()
{
  global $db_database;
  mysql_select_db($db_database)
  or die("Error de seleccion de DB ".mysql_error());
}

function crear_estados()
{
  $query="CREATE TABLE IF NOT EXISTS estados
	(
		indice int NOT NULL AUTO_INCREMENT,
		estado varchar(100),
		PRIMARY KEY (indice)
        )
        CHARACTER SET utf8 COLLATE utf8_spanish_ci";
  
  $result=mysql_query($query);
  if(!$result)
  die("Error de conexion a la DB estados ".mysql_error());
}

function llenar_estados()
{
  global $estados;
  foreach ($estados as $estado) {
    $query = "INSERT INTO estados (estado) VALUES ('".$estado."')";
    $result=mysql_query($query);
    if(!$result)
    die("Error de insercion en la DB estados ".mysql_error());
  }
}

$nombre_tabla ="datos";

function crear_datos()
{
  global $nombre_tabla;
  $query = "DROP TABLE ".$nombre_tabla;
  $result=mysql_query($query);
  if(!$result)
  die("Error de borrado de tabla datos".mysql_error());
  $query="CREATE TABLE ".$nombre_tabla."
	(
		indice int NOT NULL AUTO_INCREMENT,
		programa text,
		cve_estado int,
                estado varchar(100),
		cve_municipio int,
		municipio varchar(255),
                solicitante text,
		tipo_beneficiario varchar(255),
		predio text,
		concepto1 varchar(255),
		concepto2 varchar(255),
		superficie float(8,2),
		unidad varchar(100),
		monto double,
                anho int,
                PRIMARY KEY (indice)
      )CHARACTER SET utf8";

  $result=mysql_query($query);
  if(!$result)
  die("Error de conexion a la DB datos ".mysql_error());
}

function llenar_datos()
{
  global $nombre_tabla;
  $query ="
    LOAD DATA LOCAL INFILE 'xls2php/Base2010Final.csv'
    INTO TABLE ".$nombre_tabla."
    CHARACTER SET utf8
    FIELDS TERMINATED BY '|'
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES
    (programa, cve_estado, estado, cve_municipio, municipio, solicitante, tipo_beneficiario, predio, concepto1, concepto2, superficie, unidad, monto, anho)
    ";
  
  $result=mysql_query($query);
  if(!$result)
  die("Error de insercion en la DB datos datos ".mysql_error());
}

connect();
crear_db();
seleccionar_db();
crear_estados();
llenar_estados();
crear_datos();
llenar_datos();

mysql_close($conexion);

die("Chidou. Winners don't do drugs.");
?>
