<?php
require_once 'login.php';

if (isset($_GET["pagina"])) {
  $pagina = $_GET["pagina"];
  
} else {
  $pagina = 1;
}
/*
CCMSS
Consulta SQL 1: consulta ejemplo sobre DB importada XLS > MySQL
 */

/* html>head>title+script+script+script^body>#sql_texto+#resultados+script */
?>

<html>
  <head>
    <title>
Consulta SQL 1: consulta ejemplo sobre DB importada XLS > MySQL
    </title>
    
    
    <!-- Espacio para cargar scripts: -->
    <script>
    </script>
    <script>
    </script>
    <script>
    </script>
    <style>
     body {
       font-family:Arial;
     }
     ul li:first-child {
       /*width:1.5%;*/
     }
     li {
       display:inline-block;
       width:6.5%;
       float:left;
       font-size:10px;
       height:40px;
       overflow:hidden;
     }
     ul {
       margin:10px 0;
       padding:10px 0;
       clear:both;
       width:100%;
       min-height:30px;
     }

     .titulos {
       background-color:#333;
       color:#fff;
       font-weight:bold;
     }
     .non, .non li {
       background-color:#a0a3fa;
     }
     .par, .par li {
       background-color:#aaa;
     }

     #resultados {
       height:400px;

       overflow-y:scroll;
       overflow-x:visible;
     }

     #sql_texto {
       margin:30px 0;
       padding:20px;

       border: 3px solid #a0a3fa;
     }
     #paginacion {
       width:100%;
       height:50px;
       overflow:hidden;
     }
     #paginacion li { width:15px; margin:5px; padding:2px; font-size: 12px; overflow:visible; text-align: center; }
     #paginacion li a { text-decoration:none ; color:#666;}
     #paginacion li.actual a { color:#000; font-weight: bold;}

     #paginacion .primera { margin-right:15px;}
     #paginacion .ultima { margin-left:15px;}
    </style>
  </head>
  <body>

    <?php
    global $db_hostname,$db_username,$db_pwd;


    function curPageURL() {
      $pageURL = 'http';
      if( isset($_SERVER["HTTPS"])) {
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
      }
      $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
          $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
          $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
      
      return $pageURL;
    }



    
    $conexion=mysql_connect($db_hostname,$db_username,$db_pwd);
    if (!$conexion)
    die("Error de conexion al servidor ".mysql_error()); //El die debe reemplazarse por código de deployment
    mysql_select_db("ccmss_data") or die (mysql_error());
    $total_rows=mysql_num_rows(mysql_query("SELECT * from datos"));
    
    
    $num_pags = floor( $total_rows / 100 );

    $pagina = $pagina > $num_pags ? $num_pags : $pagina;
    $pagina = $pagina < 1 ? 1 : $pagina;
    
    $link = curPageURL();
    $link = preg_replace('/\?.*/', '', $link );
    
    $show_pags = 15;

    $pag_offset = ceil($show_pags / 2);

    $start_pag = $pagina - $pag_offset;
    $start_pag < 1 ? $start_pag = 0 : 0;
    $pagina + $pag_offset >= $num_pags ? $start_pag = $num_pags - $show_pags : 0;
    
    
    $paginacion = "";
    for($i=$start_pag;$i<$start_pag+$show_pags;$i++){
      $pag = ($i+1);
      $clase = $pag == $pagina ? 'class="actual"' : "";
      
      $paginacion .= '<li '.$clase.'><a href="'.$link.'?pagina='.$pag.'">'.$pag.'</a></li>';
    }
    $primera = $pagina > $pag_offset ?  '<li class="primera"><a href="'.$link.'?pagina=1">1</a></li>' : "";
    
    if( $pagina < $num_pags - $pag_offset ) {
      $ultima = '<li class="ultima"><a href="'.$link.'?pagina='.( $num_pags ) .'">'. ($num_pags  ).'</a></li>';
    }
    $paginacion = $primera . $paginacion . $ultima;

    $paginacion =  '<ul id="paginacion">' . $paginacion . '</ul>';
    
    if($pagina > 1 ) {
      $offset = $pagina * 100;
    }
    else {
      $offset = 0;
    }
    
     $query = "SELECT * from datos LIMIT ".$offset.",100";
     $result = mysql_query( $query );
     //$cols_query = mysql_query( "SELECT column_name USER_TAB_COLUMNS WHERE table_name = 'datos'" );
     $cols_query = mysql_query( "SELECT COLUMNS FROM datos" );

     $rows = mysql_num_rows( $result );
     $cols = 13;


     $titulos_resultados = '';
     $resultados = '';
     $titulos_resultados .= '<ul class="titulos">';
     $titulos_resultados .= '<li>-</li>';
     for( $j = 1; $j <= $cols; $j++ ) {
       $nombre_col = mysql_field_name (  $result , $j );
       $titulos_resultados .= '<li>'.$nombre_col.'</li>';
     }
     $titulos_resultados .= '</ul>';

     for( $i = 0; $i < $rows; $i++ ) {
       if( $i % 2 == 0 ) {
         $clase = "par";
       }
       else {
         $clase = "non";
       }

       $resultados .= '<ul class="'.$clase.'">';
       $resultados .= '<li>'. (int)($offset +$i +1) . '</li>';
       for( $j = 1; $j <= $cols; $j++ ) {
         $resultados .= '<li>'.mysql_result( $result, $i, $j ).'</li>';
       }
       $resultados .= '</ul>';
     }


     mysql_close($conexion);
     ?>

     <div id="descripcion">
       Una consulta SQL de ejemplo sobre la base de datos MySQL creada tras importar el archivo XLS.
     </div>

     <!-- En este espacio se muestra la consulta en texto. -->
     <div id="sql_texto">
       <?php echo $query; ?>
     </div>
     <div id="titulos_resultados">
       <?php 
       echo $titulos_resultados;
       ?>
     </div>
     <div id="paginacion">
      <?php 
      echo $paginacion
      ?>
    </div>
    
    <div id="resultados">
      <!-- Aquí se mostrarán los resultados de la consulta. -->
      <?php 
      echo $resultados;
      ?>
    </div>
    
    <script type="text/javascript">
     /*
     Javascript propio
      */
    </script>
  </body>
</html>
